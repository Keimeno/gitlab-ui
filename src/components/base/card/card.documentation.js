import * as description from './card.md';
import examples from './examples';

export default {
  description,
  examples,
  bootstrapComponent: 'b-card',
};
